package com.jeewms.www.wms.ui.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.jeewms.www.wms.bean.bean.MessageEvent;
import com.jeewms.www.wms.bean.vm.WaveToFjEntity;
import com.jeewms.www.wms.ui.itemview.WavetofjItemView;
 import com.jeewms.www.wms.util.LoadingUtil;
import com.jeewms.www.wms.util.Logutil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by 13799 on 2018/6/7.
 */

public class WavetoFjCardsAdapter extends BaseAdapter{

    Context mContext;
    private List<WaveToFjEntity> mPickingList;

    public List<WaveToFjEntity> getmPickingList() {
        return mPickingList;
    }

    public void setmPickingList(List<WaveToFjEntity> mPickingList) {
        this.mPickingList = mPickingList;
    }

    public WavetoFjCardsAdapter(Context context){
        mContext=context;
    }
    @Override
    public int getCount() {
        return mPickingList==null?0:mPickingList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        WavetofjItemView itemView=null;
        if(view==null){
            itemView=new WavetofjItemView(mContext);
            view=itemView.getView();
            view.setTag(itemView);
        }else{
            itemView= (WavetofjItemView) view.getTag();
        }
        itemView.bindView(mPickingList.get(i),i);
        itemView.setListent(new WavetofjItemView.PickingDetailListent() {
            @Override
            public void save(final int position) {
                LoadingUtil.hideLoading();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Logutil.print("cez");

                        remove(position);
                    }
                },600);
            }

            @Override
            public void setTinId2(int position, String value) {
//                mPickingList.get(position).se(value);
            }

            @Override
            public void setBinId2(int position, String value) {
//                mPickingList.get(position).setBinId(value);

            }

        });
        return view;
    }
    public void remove(int position){
        mPickingList.remove(position);
        notifyDataSetChanged();
        EventBus.getDefault().post(new MessageEvent(""));
    }



}
