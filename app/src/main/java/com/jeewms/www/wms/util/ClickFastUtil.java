package com.jeewms.www.wms.util;

/**
 * Created by 13799 on 2018/7/3.
 */


public class ClickFastUtil {
    // 两次点击按钮之间的点击间隔不能少于1000毫秒
    private static final int MIN_CLICK_DELAY_TIME = 1000;

    private static final int MIN_CLICK_DELAY_TIME3 = 3000;
    private static final int MIN_CLICK_DELAY_TIME2= 500;
    private static long lastClickTime;
    //返回true时可以进行点击，返回false不可以进行点击
    public static boolean isFastClick1000() {
        boolean flag = false;
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
            flag = true;
        }
        lastClickTime = curClickTime;
        return flag;
    }
    public static boolean isFastClick500() {
        boolean flag = false;
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME2) {
            flag = true;
        }
        lastClickTime = curClickTime;
        return flag;
    }
    public static boolean isFastClick3000() {
        boolean flag = false;
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME3) {
            flag = true;
        }
        lastClickTime = curClickTime;
        return flag;
    }
}

